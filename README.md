# DeVolta

Container image for developing Node.js apps

[![asny23/devolta](https://dockerico.blankenship.io/image/asny23/devolta)](https://hub.docker.com/r/asny23/devolta)

- Based on [bitnami/minideb](https://github.com/bitnami/minideb)
- [Volta](https://volta.sh/) installed
