FROM bitnami/minideb:bookworm

ENV VOLTA_VERSION=2.0.2 VOLTA_HOME="/usr/local/volta" PATH="$PATH:/usr/local/volta/bin"

SHELL ["/bin/bash", "-o", "errexit", "-o", "nounset", "-o", "pipefail", "-c"]
RUN \
  install_packages curl ca-certificates && \
  curl https://get.volta.sh | bash -s -- --skip-setup --version $VOLTA_VERSION

CMD ["node"]
